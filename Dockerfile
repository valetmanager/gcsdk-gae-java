FROM google/cloud-sdk:450.0.0

RUN apt-get update && apt-get install openjdk-17-jdk -y && apt-get install wget -y && apt-get install unzip -y

ARG MAVEN_VERSION=3.5.4
ARG USER_HOME_DIR="/root"
ARG SHA=ce50b1c91364cb77efe3776f756a6d92b76d9038b0a0782f7d53acf1e997a14d
ARG BASE_URL=https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/${MAVEN_VERSION}/apache-maven-${MAVEN_VERSION}-bin.tar.gz

RUN mkdir -p /usr/share/maven /usr/share/maven/ref && \
	curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL} && \
	echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - && \
	tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 && \
	rm -f /tmp/apache-maven.tar.gz && \
	ln -s /usr/share/maven/bin/mvn /usr/bin/mvn && \
	wget https://services.gradle.org/distributions/gradle-7.5.1-bin.zip && \
	mkdir /opt/gradle && \
	unzip -d /opt/gradle gradle-7.5.1-bin.zip && \
	export PATH=$PATH:/opt/gradle/gradle-7.5.1/bin && \
	export GRADLE_OPTS=-Xmx1000m &&\
	curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
	apt-get update -qq && apt-get install -qq --no-install-recommends nodejs npm yarn && rm -rf /var/lib/apt/lists/* && \
	apt-get update && apt-get install -y libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 xvfb
ENV MAVEN_HOME=/usr/share/maven MAVEN_CONFIG="$USER_HOME_DIR/.m2"
CMD mvn -Dapp.devserver.host="0.0.0.0" appengine:run
